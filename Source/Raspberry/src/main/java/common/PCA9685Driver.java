package common;

import java.io.IOException;
import java.util.ArrayList;
import com.pi4j.io.i2c.*;
import com.pi4j.io.i2c.I2CFactory.UnsupportedBusNumberException;

public class PCA9685Driver
{
	static private ArrayList<PCA9685Driver> drivers = new ArrayList<PCA9685Driver>();
	
	static private byte DEFAULT_I2CADDRESS = 0x40;
	static private int DEFAULT_FREQUENCY = 50; //Hz
	
	static private final byte MODE1_REG = (byte) 0x00;
	static private final byte PRESCALE_REG = (byte) 0xFE;
	static private final byte LED0_ON_L_REG = (byte) 0x06;
	static private final byte LED0_ON_H_REG = (byte) 0x07;
	static private final byte LED0_OFF_L_REG = (byte) 0x08;
	static private final byte LED0_OFF_H_REG = (byte) 0x09;
	static private final int MAX_LED_12BIT_REG_VAL = 4095;
	
	private byte i2cAddress = 0x00;
	private int pwmFrequency = 0;
	
	private boolean open = false;
	private I2CDevice deviceHandle = null;
	
	private PCA9685Driver(byte address, int frequency)
	{
		i2cAddress = address;
		pwmFrequency = frequency;
	}
	
	static public PCA9685Driver createDefault()
	{
		if(driverExists(DEFAULT_I2CADDRESS))
		{
			return retrieveDriver(DEFAULT_I2CADDRESS);
		}
		else
		{
			return new PCA9685Driver(DEFAULT_I2CADDRESS, DEFAULT_FREQUENCY);
		}
	}
	
	static public PCA9685Driver create(byte address)
	{
		PCA9685Driver driver = retrieveDriver(address);
		if(driver != null)
		{
			return driver;
		}
		else
		{
			driver = new PCA9685Driver(address, DEFAULT_FREQUENCY);
			drivers.add(driver);
			return driver;
		}
	}
	
	public boolean open()
	{
		try 
		{
			I2CBus i2c = I2CFactory.getInstance(I2CBus.BUS_2);
			deviceHandle = i2c.getDevice(i2cAddress);
		} 
		catch (UnsupportedBusNumberException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		if(!driverExists(i2cAddress)) //in case driver is reopened after close() has been used
		{
			drivers.add(this);
		}
		
		open = (null != deviceHandle);

		return open;
	}
	
	public boolean close()
	{
		if(null != deviceHandle) deviceHandle = null;
		open = false;
		drivers.remove(this);
		return open;
	}
	
	public byte getAddress() { return i2cAddress; }
	public int getPwmFrequency() { return pwmFrequency; }
	public boolean isOpen() { return open; }
	
	public void setPwmFrequency(int hz)
	{
		pwmFrequency = hz;
		
		try
		{
			if(isOpen())
			{
				byte mode1 = (byte) (deviceHandle.read(MODE1_REG) & 0xFF);
				byte updatedMode1 = (byte) ((mode1 & (byte) 0x7F) | (byte) 0x10); //set reset bit MSB to 0, set sleep bit to 1
				deviceHandle.write(MODE1_REG, updatedMode1);
				deviceHandle.write(PRESCALE_REG, (byte) (calculatePrescaler(hz) & 0xFF)); //set prescaler
				Thread.sleep(0, 500000); //wait 500 microseconds for oscllator to stabilize
				deviceHandle.write(MODE1_REG, (byte) (mode1 | (byte) 0x80)); //set restart bit to 1
			}
		}
		catch(IOException ex)
		{
			ex.printStackTrace();
		}
		catch (InterruptedException ex) 
		{
			ex.printStackTrace();
		}
	}
	
	public void setPWM(byte channel, int dutyPercentage)
	{
		int duty = normalizePercents(dutyPercentage);
		int onValue = 0; //always high since timer 0
		int offValue = (duty * MAX_LED_12BIT_REG_VAL) / 100;
		
		if(isOpen())
		{
			try
			{
				deviceHandle.write(LED0_ON_L_REG + 4 * channel, (byte) onValue);
				deviceHandle.write(LED0_ON_H_REG + 4 * channel, (byte) onValue);
				deviceHandle.write(LED0_OFF_L_REG + 4 * channel, (byte) (offValue & 0xFF));
				deviceHandle.write(LED0_OFF_H_REG + 4 * channel, (byte) ((offValue >> 8) & 0x0F));
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	private int normalizePercents(int percents)
	{
		int percentsNormalized = 0;
		
		if(percents < 0) percentsNormalized = 0;
		else if(percents > 100) percentsNormalized = 100;
		else percentsNormalized = percents;
		
		return percentsNormalized;
	}
	
	private int calculatePrescaler(int frequency) //Hz
	{
		int oscillatorClock = 25 * 1000 * 1000; //25MHz
		return Math.round((oscillatorClock / (4096 * frequency)) - 1);
	}
	
	static private boolean driverExists(byte address)
	{
		for(PCA9685Driver driver : drivers)
		{
			if(driver.getAddress() == address)
			{
				return true;
			}			
		}		
		return false;
	}
	
	static private PCA9685Driver retrieveDriver(byte address)
	{
		for(PCA9685Driver driver : drivers)
		{
			if(driver.getAddress() == address)
			{
				return driver;
			}			
		}
		return null;
	}
}
