package robot;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.api.RequestParameters;
import io.vertx.ext.web.api.contract.RouterFactoryOptions;
import io.vertx.ext.web.api.contract.openapi3.OpenAPI3RouterFactory;
import io.vertx.ext.web.Router;
import io.vertx.core.Future;
import io.vertx.ext.web.handler.BodyHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPin;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinDirection;
import com.pi4j.io.gpio.PinMode;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.trigger.GpioCallbackTrigger;
import com.pi4j.io.gpio.trigger.GpioPulseStateTrigger;
import com.pi4j.io.gpio.trigger.GpioSetStateTrigger;
import com.pi4j.io.gpio.trigger.GpioSyncStateTrigger;
import com.pi4j.io.gpio.event.GpioPinListener;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import com.pi4j.io.gpio.event.PinEventType;


public class Robot extends AbstractVerticle
{
    private HttpServer server;
    GpioController gpio;
    GpioPinDigitalOutput myLed;
    public static void main(String[] args)
    {
        System.out.println("Hello world");
        System.out.println("WTF");
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new Robot());
    }

    @Override
    public void start()
    {
        gpio = GpioFactory.getInstance();
        myLed = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_07,   // PIN NUMBER
                "My LED",           // PIN FRIENDLY NAME (optional)
                PinState.LOW);
        //setUpInitialData(); set up connection to stm, load all perypherals like temperature sensor etc.
        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        router.put("/vehicle").handler(this::handleVehicleMovement);
        router.put("/camera").handler(this::handleCameraMovement);
        router.get("/camera").handler(this::handleGetCameraConfig);
        router.get("/telemetry").handler(this::handleGetTelemetry);

        server = vertx.createHttpServer().requestHandler(router).listen(45450);
    }

    @Override
    public void stop()
    {
        //disconnect mcu - stop all movement etc
        server.close();
    }

    private void handleVehicleMovement(RoutingContext rc)
    {
        myLed.high();
        rc.response().setStatusCode(201).setStatusMessage("Acknowledged").end();
    }

    private void handleCameraMovement(RoutingContext rc)
    {
        myLed.low();
        rc.response().setStatusCode(201).setStatusMessage("Acknowledged").end();
    }

    private void handleGetCameraConfig(RoutingContext rc)
    {
        rc.response().setStatusCode(200).setStatusMessage("OK").end();
    }

    private void handleGetTelemetry(RoutingContext rc)
    {
        rc.response().setStatusCode(200).setStatusMessage("OK").end();
    }
}
