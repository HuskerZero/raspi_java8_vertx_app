package interfaces;

public interface ILandVehicleControl 
{
	public void moveForward(float movePercentage);
	public void moveSideways(float movePercentage);
	public boolean isMoving();
}
