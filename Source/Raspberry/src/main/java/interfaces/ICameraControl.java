package interfaces;

import io.vertx.core.json.JsonObject;

public interface ICameraControl 
{
	public void moveVertical(float movePercentage);
	public void moveHorizontal(float movePercentage);
	public boolean isOperational();
	public boolean init();
	public JsonObject getParameters();
	public void setParameters(JsonObject parameters);
}
